	$(document).ready(function() {
	  	$('#main-slider').flexslider({
			animation: "fade",
			directionNav: true,
			controlNav: true, 
			easing: "swing", 
			direction: "horizontal",
			controlsContainer: ".flex-container",
			before: function(){
				$('.flex-caption').animate({'left':'0px','opacity':'0'},0,'easeOutBack'); 
				},
				after: function(){
					$('.flex-caption').animate({'left':'50px','opacity':'1'},800,'easeOutBack'); 
					},			
		});
				
		
	});	
	$(document).ready(function(){
		$(".top_social li a, ul.client-list li a img").css({"opacity": "1"});
		$(".top_social li a, ul.client-list li a img").hover(function() {
			$(this).stop().animate({opacity: 0.5,}, 300 );
		},
		function() {
			$(this).stop().animate({opacity: 1,}, 300 );
		});
	});
	$(document).ready(function() {
		$('#secondary-slider').flexslider({
			animation: "slide",
			slideshow : false,
			controlNav: false,
			direction: "horizontal", 
			smoothHeight : true,
			controlsContainer : '.testimonials_nav'
		});
	});
		
		
	
	$(document).ready(function() { 
	   
      // Create the dropdown base
      $("<select />").appendTo("nav");
      
      // Create default option "Go to..."
      $("<option />", {
         "selected": "selected",
         "value"   : "",
         "text"    : "Go to..."
      }).appendTo("nav select");
      
      // Populate dropdown with menu items
      $("nav a").each(function() {
       var el = $(this);
       $("<option />", {
           "value"   : el.attr("href"),
           "text"    : el.text()
       }).appendTo("nav select");
      });
      
	   // To make dropdown actually work
	   // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
      $("nav select").change(function() {
        window.location = $(this).find("option:selected").val();
      });
	 
	 });
	
	//INITIALIZW MAIN MENU
	$(document).ready(function() { 
		$('.sf-menu').superfish({ 
			delay:       500,                            // one second delay on mouseout 
			animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
			speed:       'fast',                          // faster animation speed
			autoArrows:  false,                           // disable generation of arrow mark-up 
			dropShadows: false                            // disable drop shadows 
		}); 
	  /*hack for a bug in Chrome with the background when the dropdown is animated*/
		if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
			$('.sf-menu').superfish({ 
				delay:       500,
				animation:   {opacity:'show',height:'show'},
				speed:       0,
				autoArrows:  false,
				dropShadows: false
			}); 
		}
	});
	
	////PORTFOLIO ITEM HOVER
//	jQuery(document).ready(function($) {
//		/*Image hover*/
//		$('.hover-img').hover(function() {
//			$(this).find('i').stop().animate({
//				opacity: 0.5
//			}, 'fast');
//			$(this).find('a').stop().animate({
//				"top": "50%"
//			});
//		}, function() {
//			$(this).find('i').stop().animate({
//				opacity: 0
//			}, 'fast');
//			$(this).find('a').stop().animate({
//				"top": "-30px"
//			});
//			
//		}); 
//	});

	//INITIALIZES LIGHTBOX PLUGIN
	$(document).ready(function() {
		$("a[rel^='prettyPhoto']").prettyPhoto({
			opacity:0.80,
			default_width:500,
			default_height:344,
			theme:'dark_rounded',
			hideflash:false,
			modal:false,
			showTitle: false,
		});
	});

$(document).ready(function() {
		$(".hover-img img").hover(function(){
			$(this).stop().animate({opacity: "0.5"}, 350);
		},
		function(){
			$(this).stop().animate({opacity: "1"}, 125);
		});
		$('#foo4').carouFredSel({
					responsive: true,
					width: '100%',
					scroll: 1,
					pagination: "#pager2",
					items: {
						width: 400,
						height: '100%',	//	optionally resize item-height
						visible: {
							min: 1,
							max: 1,
						}
					}
				});
	});
	
	//PORTFOLIO ZOOM ICON HOVER
	//$(document).ready(function(){
//		$(".fade-img").css({"opacity": "0.5"});
//		$(".fade-img").hover(function() {
//			$(this).stop().animate({top: "-10px"}, 300);
//			$(this).stop().animate({"opacity": "1"},  "medium");
//			$(this).stop().animate({opacity: 1}, 'fast' );
//		},
//		function() {
//			$(this).stop().animate({top: "0px"}, 300);		  
//			$(this).stop().animate({"opacity": "0.5"},  "slow");
//			$(this).stop().animate({opacity: 0.7}, 'fast' );
//		});
//	});
	
	
	
	
	//INITIALIZES BACK TO TOP
	$(document).ready(function(){
		// hide #back-top first
		$("#back-top").hide();		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});	
			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});	
	});
	
	//INITIALIZES TWITTER FEED PLUGIN
	$(document).ready(function() { 
		$(".tweet").tweet({
			username: "seaofclouds",//Change with your own twitter id
			//join_text: "auto",
			//avatar_size: 32,
			count: 3,
			//auto_join_text_default: "we said,",
			//auto_join_text_ed: "we",
			//auto_join_text_ing: "we were",
			//auto_join_text_reply: "we replied to",
			//auto_join_text_url: "we were checking out",
			loading_text: "loading tweets..."
		});		
	});
	
	
	$(document).ready(function() {
	var $zcarousel = $('#our-blog, #our-work');
	
		if( $zcarousel.length ) {
	
			var scrollCount;
			var itemWidth;
	
			if( $(window).width() < 479 ) {
					scrollCount = 1;
					itemWidth = 300;
				} else if( $(window).width() < 768 ) {
					scrollCount = 1;
					itemWidth = 200;
				} else if( $(window).width() < 960 ) {
					scrollCount = 4;
					itemWidth = 172;
				} else {
					scrollCount = 4;
					itemWidth = 220;
			}
	
			$zcarousel.jcarousel({
				   easing: 'easeInOutQuint',
				   animation : 800,
				   scroll    : scrollCount,
				   setupCallback: function(carousel) {
				   carousel.reload();
					},
					reloadCallback: function(carousel) {
						var num = Math.floor(carousel.clipping() / itemWidth);
						carousel.options.scroll = num;
						carousel.options.visible = num;
					}
				});
			}
	});  
	
	// CONTACT FORM VALIDATION
	//$(document).ready(function(){
//		$("#contact_form").validate({
//			meta: "validate",
//			submitHandler: function (form) {
//				$('#contact_form').hide();
//				$('#sucessmessage').append("<h4 class='form_thanks'>Thanks! Our Team will get in touch in next 24 hours</h4>");
//				return false;
//				form.submit();
//			},
//			 
//			rules: {
//				name: "required",
//				
//				lastname: "required",
//				 simple rule, converted to {required:true}
//				email: { // compound rule
//					required: true,
//					email: true
//				},
//				subject: {
//					required: true,
//				},
//				comment: {
//					required: true
//				}
//			},
//			messages: {
//				name: "*",
//				lastname: "*",
//				email: {
//					required: "*",
//					email: "*"
//				},
//				subject: "*",
//				comment: "*"
//			},
//		});
//	});	
	
	$(document).ready(function(){
		/* tabs
		-------------------------------*/
		$('.tabs').each(function(){
			$(this).find('.tabs-control a:first').addClass('active');
			$(this).find('.tabs-tabs .tabs-tab:first').addClass('active');
		});
		
		$('.tabs .tabs-control a').click(function(){
			var $tabs=$(this).parents('.tabs');
			if(!$tabs.length)
				return false;
				
			var tabname=$(this).attr('href').replace('#','');
			
			$tabs.find('.tabs-control a').removeClass('active');
			$(this).addClass('active');

			$tabs.find('.tabs-tabs .tabs-tab.active').hide().removeClass('active');
			$tabs.find('.tabs-tabs .tabs-tab.'+tabname).addClass('active').fadeIn(300);
			
			
			return false;
		});
		
		/* toggle
		-------------------------------*/
		$(".toggle-title").each(function(){
			if(!$(this).parent().hasClass("active")){
				$(this).next().hide();
			}
			$(this).click(function(){
					$(this).next().slideToggle(Math.min(600 , Math.max(300 , $(this).next().height())));
					$(this).parent().toggleClass("active");
					return false;
			});
		});
		
		/* accordion
		-------------------------------*/
		$(".accordion-panel-title").each(function(){
			if(!$(this).parent().hasClass("active")){
				$(this).next().hide();
			}
			
			$(this).click(function(){
				if($(this).next().is(":hidden")){
					$(this).parent().parent().find(".accordion-panel-title").next().slideUp(Math.min(600 , Math.max(300 , $(this).next().height()))).parent().removeClass("active");
					$(this).next().slideDown(Math.min(600 , Math.max(300 , $(this).next().height()))).parent().toggleClass("active");
				}
				
				return false;
			});
		});
	});
	
	 
	
	//INITIALIZW TESTIMONIAL QUOTES
//	$(document).ready(function() {
//		$('blockquote').quovolver();		
//	});
//	
//	
//	INITIALIZW TOOLTIP JQUERY
//	$(document).ready(function() {
//		$('a.tipsy').tipsy({	
//			delayIn: 100,      // delay before showing tooltip (ms)
//			delayOut: 500,     // delay before hiding tooltip (ms)
//			fade: true,     // fade tooltips in/out?
//			fallback: '',    // fallback text to use when no tooltip text
//			gravity: 's',    // gravity
//			html: true,     // is tooltip content HTML?
//			live: false,     // use live event support?
//			offset: 15,       // pixel offset of tooltip from element
//			opacity: 0.8,    // opacity of tooltip
//			title: 'title',  // attribute/callback containing tooltip text
//			trigger: 'hover' // how tooltip is triggered - hover | focus | manual	
//		});
//	});
//	
//	PORTFOLIO ITEM HOVER
//	jQuery(document).ready(function($) {
//		Image hover
//		$('.hover-img').hover(function() {
//			$(this).find('i').stop().animate({
//				opacity: 0.5
//			}, 'fast');
//			$(this).find('a').stop().animate({
//				"top": "50%"
//			});
//		}, function() {
//			$(this).find('i').stop().animate({
//				opacity: 0
//			}, 'fast');
//			$(this).find('a').stop().animate({
//				"top": "-30px"
//			});
//			
//		}); 
//	});
//	
//	PORTFOLIO ZOOM ICON HOVER
//	$(document).ready(function(){
//		$(".fade-img").css({"opacity": "0.5"});
//		$(".fade-img").hover(function() {
//			$(this).stop().animate({top: "-10px"}, 300);
//			$(this).stop().animate({"opacity": "1"},  "medium");
//			$(this).stop().animate({opacity: 1}, 'fast' );
//		},
//		function() {
//			$(this).stop().animate({top: "0px"}, 300);		  
//			$(this).stop().animate({"opacity": "0.5"},  "slow");
//			$(this).stop().animate({opacity: 0.7}, 'fast' );
//		});
//	});
//	
//	INITIALIZES LIGHTBOX PLUGIN
//	$(document).ready(function() {
//		$("a[rel^='prettyPhoto']").prettyPhoto({
//			opacity:0.80,
//			default_width:500,
//			default_height:344,
//			theme:'dark_rounded',
//			hideflash:false,
//			modal:false,
//			showTitle: false,
//		});
//	});
//	INITIALIZES TWITTER FEED PLUGIN
//	$(document).ready(function() { 
//		$(".tweet").tweet({
//			username: "seaofclouds",//Change with your own twitter id
//			join_text: "auto",
//			avatar_size: 32,
//			count: 3,
//			auto_join_text_default: "we said,",
//			auto_join_text_ed: "we",
//			auto_join_text_ing: "we were",
//			auto_join_text_reply: "we replied to",
//			auto_join_text_url: "we were checking out",
//			loading_text: "loading tweets..."
//		});		
//	});
//	
//	INITIALIZES BACK TO TOP
//	$(document).ready(function(){
//		 hide #back-top first
//		$("#back-top").hide();		
//		 fade in #back-top
//		$(function () {
//			$(window).scroll(function () {
//				if ($(this).scrollTop() > 100) {
//					$('#back-top').fadeIn();
//				} else {
//					$('#back-top').fadeOut();
//				}
//			});	
//			 scroll body to 0px on click
//			$('#back-top a').click(function () {
//				$('body,html').animate({
//					scrollTop: 0
//				}, 800);
//				return false;
//			});
//		});	
//	});
//	$(document).ready(function(){
//		 tabs
//		-------------------------------
//		$('.tabs').each(function(){
//			$(this).find('.tabs-control a:first').addClass('active');
//			$(this).find('.tabs-tabs .tabs-tab:first').addClass('active');
//		});
//		
//		$('.tabs .tabs-control a').click(function(){
//			var $tabs=$(this).parents('.tabs');
//			if(!$tabs.length)
//				return false;
//				
//			var tabname=$(this).attr('href').replace('#','');
//			
//			$tabs.find('.tabs-control a').removeClass('active');
//			$(this).addClass('active');
//
//			$tabs.find('.tabs-tabs .tabs-tab.active').hide().removeClass('active');
//			$tabs.find('.tabs-tabs .tabs-tab.'+tabname).addClass('active').fadeIn(300);
//			
//			
//			return false;
//		});
//		
//		 toggle
//		-------------------------------
//		$(".toggle-title").each(function(){
//			if(!$(this).parent().hasClass("active")){
//				$(this).next().hide();
//			}
//			$(this).click(function(){
//					$(this).next().slideToggle(Math.min(600 , Math.max(300 , $(this).next().height())));
//					$(this).parent().toggleClass("active");
//					return false;
//			});
//		});
//		
//		 accordion
//		-------------------------------
//		$(".accordion-panel-title").each(function(){
//			if(!$(this).parent().hasClass("active")){
//				$(this).next().hide();
//			}
//			
//			$(this).click(function(){
//				if($(this).next().is(":hidden")){
//					$(this).parent().parent().find(".accordion-panel-title").next().slideUp(Math.min(600 , Math.max(300 , $(this).next().height()))).parent().removeClass("active");
//					$(this).next().slideDown(Math.min(600 , Math.max(300 , $(this).next().height()))).parent().toggleClass("active");
//				}
//				
//				return false;
//			});
//		});
//	});
//	